# Introduction
Cet atelier a été conçu à partir d'une application conçue lors du cours 420-650-AL (séminaire de sujets avancés en informatique). Cet atelier ne couvre que la couche de persistence de l'application mais si vous souhaitez en appprendre davantage sur JSF vous pouvez consulter [cette introduction.](https://bitbucket.org/jayfaim/seminaire_jsf/src/master/)

# Plan de présentation
1. Présentation de Morphia
2. Démonstration de l'implantation de Morphia/MongoDB à partir d'une application utilisant Hibernate/H2

# Morphia
## Qu'est-ce que Morphia?
[Morphia](https://morphia.dev/) est une librairie open source permettant de faire le lien entre une base de données MongoDB (qui utilise un modèle à base de documents) et les objets du domaine d'affaire en Java (qui utilisent le modèle orienté-objet). Cet ODM peut être intéressant comme alternative à SpringData si l'application n'utilise pas Spring.

## Annotations
Il y a 4 annotations qui sont importantes et utilisé régulièrement dans Morphia:

@Entity : C'est utilisé pour spécifier un nom pour la collection ou l'entité sera stocké. Si on ne met pas l'annotation ou si on ne spécifie pas de nom, la collection aura le même nom que la classe.
@Embedded : Cette annotation sert à spécifier à Morphia que la classe qui porte cette annotation sera intégré dans un tableau dans le document de la classe parent spécifié.
@Id : Cette annotation est absolument requis. Elle sert à spécifier la variable d'instance qui sera utilisé comme identifiant dans mongoDB.
@Reference : Cette annotation sert à spécifier une référence à un document qui est stocké dans une notre collection. Par défaut, cette référence sera représenter avec une valeur DBRef dans mongoDB. Par contre, on peut spécifier dans l'annotation de seulement stocker le id de la référence à la place de utilisé un DBRef avec (idOnly = true).

## Queries
Pour faire des select, il faut commencer par créer le datastore.
```
Morphia morphia = new Morphia();
Datastore datastore = morphia.createDatastore(new MongoClient(), NOM_DB);
```
Ensuite on créer la query en lui donnant la classe où on fait le select.
```
Query<Objet> query = datastore.createQuery(Objet.class);
```
Ensuite pour filtrer (WHERE) il y a 2 façons pour le faire; soit avec filter, ou soit avec field.
```
query.filter("valeurNumerique >=", 5);
query.field("valeurNumerique").greaterThanOrEq(5);
```
Finalement, pour obtenir le résultat, on utilise la méthode find. la méthode find retourne un pointeur sur une liste qui contient les résultats. pour obtenir les résultats, on peut soit utiliser la méthode next() pour obtenir le prochain dans la liste, ou on peut utiliser la méthode toList() pour obtenir la liste au complet.
```
Objet ob = datastore.createQuery(Objet.class).filter("id ==", 1).find.next();
List<Objet> objets = datastore.createQuery(Objet.class).find().toList();
```

## Insertion
Pour faire des insertions, il faut commencer par créer le datastore.
```
Morphia morphia = new Morphia();
Datastore datastore = morphia.createDatastore(new MongoClient(), NOM_DB);
```
Ensuite, on donne au datastore l'objet à insérer.
```
datastore.save(objetASave);
```
Et puis ça va insérer.

# Démonstration
- Présentation de l'application utilisant Hibernate/H2
- Présentation de l'application utilisant Morphia/MongoDB

# Sources
- https://dzone.com/articles/using-morphia-map-java-objects