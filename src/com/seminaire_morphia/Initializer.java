package com.seminaire_morphia;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import com.seminaire_morphia.dao.TodoDao;
import com.seminaire_morphia.entities.Todo;

@WebListener
public class Initializer implements ServletContextListener {

	private static TodoDao dao;
	private static final boolean DROP_CREATE = true;

	public Initializer() {
		dao = new TodoDao();
		if(DROP_CREATE) dao.deleteAll();
	}

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		dao.save(new Todo("Titre 1", "Description 1",
				Date.from(LocalDateTime.now().minusDays(2).atZone(ZoneId.systemDefault()).toInstant())));
		dao.save(new Todo("Titre 2", "Description 2",
				Date.from(LocalDateTime.now().plusDays(1).atZone(ZoneId.systemDefault()).toInstant())));
		dao.save(new Todo("Titre 3", "Description 3",
				Date.from(LocalDateTime.now().plusDays(3).atZone(ZoneId.systemDefault()).toInstant())));
		dao.save(new Todo("Titre 4", "Description 4",
				Date.from(LocalDateTime.now().minusDays(4).atZone(ZoneId.systemDefault()).toInstant())));
		dao.save(new Todo("Titre 5", "Description 5", null));
		dao.save(new Todo("Titre 6", "Description 6",
				Date.from(LocalDateTime.now().plusWeeks(1).atZone(ZoneId.systemDefault()).toInstant())));
	}

}
