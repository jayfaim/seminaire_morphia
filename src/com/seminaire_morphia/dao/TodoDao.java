package com.seminaire_morphia.dao;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;

import com.mongodb.MongoClient;
import com.seminaire_morphia.entities.Todo;

import dev.morphia.Datastore;
import dev.morphia.Morphia;

public class TodoDao {

	private static final String DB_NAME = "seminaire_morphia";
	private Morphia morphia;
	private Datastore datastore;

	public TodoDao() {
		morphia = new Morphia();
		datastore = morphia.createDatastore(new MongoClient(), DB_NAME);
		morphia.mapPackage("com.seminaire_morphia.entities");
	}

	public Todo save(Todo todo) {
		try {
			datastore.save(todo);
			return todo;
		} catch (Exception e) {
			System.err.println("Error creating todo: " + e.getMessage());
		}
		return null;
	}

	public Todo findById(ObjectId id) {
		try {
			return datastore.createQuery(Todo.class).filter("_id ==", id).find().next();
		} catch (Exception e) {
			System.err.println("Error finding todo by id: " + e.getMessage());
		}
		return null;
	}

	public void delete(Todo todo) {
		try {
			datastore.delete(datastore.createQuery(Todo.class).filter("_id ==", todo.getId()));
		} catch (Exception e) {
			System.err.println("Error deleting todo: " + e.getMessage());
		}
	}

	public void deleteAll() {
		try {
			datastore.delete(datastore.createQuery(Todo.class));
		} catch (Exception e) {
			System.err.println("Error deleting todo: " + e.getMessage());
		}
	}

	public List<Todo> findAll() {
		try {
			return datastore.createQuery(Todo.class).find().toList();
		} catch (Exception e) {
			System.err.println("Error finding all users: " + e.getMessage());
		}
		return new ArrayList<Todo>();
	}
}
