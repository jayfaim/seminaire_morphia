package com.seminaire_morphia.managedBeans;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.bson.types.ObjectId;

import com.seminaire_morphia.dao.TodoDao;
import com.seminaire_morphia.entities.Todo;

@Named
@ViewScoped
public class TodoBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private String id;
	private Todo todo;
	private TodoDao dao;

	@PostConstruct
	public void init() {
		dao = new TodoDao();
	}

	public void retrieve() {
		if (null == id) {
			todo = new Todo();
		} else {
			todo = dao.findById(new ObjectId(id));
		}
	}

	public String save() {
		todo = dao.save(todo);
		return "view.xhtml?faces-redirect=true&id=" + todo.getId();
	}

	public String delete() {
		dao.delete(todo);
		return "index.xhtml?faces-redirect=true";
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Todo getTodo() {
		return todo;
	}

	public void setTodo(Todo todo) {
		this.todo = todo;
	}
}
