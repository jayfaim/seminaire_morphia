package com.seminaire_morphia.managedBeans;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.bson.types.ObjectId;

import com.seminaire_morphia.dao.TodoDao;
import com.seminaire_morphia.entities.Todo;

@Named
@ViewScoped
public class IndexBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private List<Todo> todos;
	private TodoDao dao;

	@PostConstruct
	public void init() {
		dao = new TodoDao();
		todos = dao.findAll();
	}

	public String create() {
		return "create.xhtml?faces-redirect=true";
	}

	public String view(ObjectId id) {
		return "view.xhtml?faces-redirect=true&id=" + id;
	}

	public List<Todo> getTodos() {
		return todos;
	}

	public void setTodos(List<Todo> todos) {
		this.todos = todos;
	}
}
